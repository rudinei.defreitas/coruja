package br.ufpa.laps.jlapsapi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationManager {

	public static Properties configurations = new Properties();
	private static String userdir = System.getProperty ("user.home");

	private static final String CONFIGURATIONS_FILE_DIRECTORY = "META-INF";
	private static final String CONFIGURATIONS_FILE_NAME = "configuration.properties";

	public static final String DEFAULT_JULIUS_CONFIGURATIONS_FILE_PATH = "/opt/coruja/julius.jconf";
	public static final String ALTERNATIVE_JULIUS_CONFIGURATIONS_FILE_PATH = userdir+"/coruja1.5_for_speechoo/julius.jconf";

	public static final String JULIUS_CONFIGURATIONS_FILE_PATH = "JULIUS_CONFIGURATIONS_FILE_DIRECTORY";

	static {
		if(!ConfigurationManager.loadConfigurationFile()){
			ConfigurationManager.createConfigurationFileDirectory();
			ConfigurationManager.createConfigurationFile();
			ConfigurationManager.loadConfigurationFile();
		}
	}

	private static boolean createConfigurationFile() {
		System.out.println("*jlapsapi createConfigurationFile");
		JarOrProjectRunnerSelector joprs = new JarOrProjectRunnerSelector();
		boolean sucess = false;

		FileOutputStream file = null;

		if(joprs.isJar()){

			if (!new File(CONFIGURATIONS_FILE_DIRECTORY + File.separator + CONFIGURATIONS_FILE_NAME).exists()) {
				try {
					file = new FileOutputStream(CONFIGURATIONS_FILE_DIRECTORY + File.separator + CONFIGURATIONS_FILE_NAME);
					configurations.put("JULIUS_CONFIGURATIONS_FILE_DIRETORY", DEFAULT_JULIUS_CONFIGURATIONS_FILE_PATH);
					configurations.put("JULIUS_CONFIGURATIONS_FILE_DIRETORY", ALTERNATIVE_JULIUS_CONFIGURATIONS_FILE_PATH);

					configurations.store(file, "No Comments");
					file.flush();
					file.close();

					System.out.println("Configuration file created on path "+
							CONFIGURATIONS_FILE_DIRECTORY+ File.separator+CONFIGURATIONS_FILE_NAME);

					sucess = true;
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}

			return sucess;
			
		} else {
			
			if (!new File(CONFIGURATIONS_FILE_DIRECTORY + File.separator + CONFIGURATIONS_FILE_NAME).exists()) {
				try {
					file = new FileOutputStream(CONFIGURATIONS_FILE_DIRECTORY + File.separator + CONFIGURATIONS_FILE_NAME);
					configurations.put("JULIUS_CONFIGURATIONS_FILE_DIRETORY", DEFAULT_JULIUS_CONFIGURATIONS_FILE_PATH);
					configurations.put("JULIUS_CONFIGURATIONS_FILE_DIRETORY", ALTERNATIVE_JULIUS_CONFIGURATIONS_FILE_PATH);

					configurations.store(file, "No Comments");
					file.flush();
					file.close();

					System.out.println("Configuration file created on path "+
							CONFIGURATIONS_FILE_DIRECTORY+ File.separator+CONFIGURATIONS_FILE_NAME);

					sucess = true;
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		}
		return sucess;
	}

	private static boolean loadConfigurationFile() {
		System.out.println("*jlapsapi loadConfigurationFile");
		boolean sucess = false;

		try {
			FileInputStream in = new FileInputStream(CONFIGURATIONS_FILE_DIRECTORY + File.separator + CONFIGURATIONS_FILE_NAME);
			configurations.load(in);
			in.close();
			sucess = true;
		} catch (FileNotFoundException fileNotFoundException) {
			System.out.println("Missing configuration file.");
		} catch (IOException IOexception) {
			IOexception.printStackTrace();
		}

		return sucess;
	}

	private static boolean createConfigurationFileDirectory() {
		System.out.println("*jlapsapi createConfigurationFileDirectory");
		boolean sucess = false;

		try {
			if (!new File(CONFIGURATIONS_FILE_DIRECTORY).exists()) {
				new File(CONFIGURATIONS_FILE_DIRECTORY).mkdir();

				File defaultConfigurationFileDirectory = new File(CONFIGURATIONS_FILE_DIRECTORY);
				defaultConfigurationFileDirectory.mkdir();

				sucess = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sucess;
	}

	public static String getProperty(String propertyName){
		return configurations.getProperty(propertyName);
	}

}
