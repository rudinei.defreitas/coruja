package br.ufpa.laps.jlapsapi.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

//Extract files directly from inside jlapsapi.jar

public class ZipExtractor {
	
	@SuppressWarnings("unchecked")
	public static void extrairZip( File arquivoZip, File diretorio ) throws ZipException, IOException {  
		ZipFile zip = null;
		File arquivo = null;
		InputStream is = null;
		OutputStream os = null;
		byte[] buffer = new byte[2048];
		try {

			if( !diretorio.exists() ) { 
				diretorio.mkdirs();
			}
			if( !diretorio.exists() || !diretorio.isDirectory() ) {
				throw new IOException("Diretório inválido");
			}
			zip = new ZipFile( arquivoZip );
			Enumeration e = zip.entries();
			while( e.hasMoreElements() ) {
				ZipEntry entrada = (ZipEntry) e.nextElement();
				if(entrada.getName().startsWith("gramtools/") || entrada.getName().startsWith("jlapsapiLibPath/")){
					arquivo = new File( diretorio, entrada.getName() );
					//se for diretório inexistente, cria a estrutura   
					//e pula pra próxima entrada  
					if( entrada.isDirectory() && !arquivo.exists() ) {
						arquivo.mkdirs();
						continue;
					}
					//se a estrutura de diretórios não existe, cria  
					if( !arquivo.getParentFile().exists() ) {  
						arquivo.getParentFile().mkdirs();  
					}  
					try {  
						//lê o arquivo do zip e grava em disco  
						is = zip.getInputStream( entrada );  
						os = new FileOutputStream( arquivo );  
						int bytesLidos = 0;  
						if( is == null ) {  
							throw new ZipException("Erro ao ler a entrada do zip: "+entrada.getName());  
						}  
						while( (bytesLidos = is.read( buffer )) > 0 ) {  
							os.write( buffer, 0, bytesLidos );  
						}  
					} finally {  
						if( is != null ) {  
							try {  
								is.close();  
							} catch( Exception ex ) {}  
						}  
						if( os != null ) {  
							try {  
								os.close();  
							} catch( Exception ex ) {}  
						}  
					} 
				}
			}  
		} finally {  
			if( zip != null ) {  
				try {  
					zip.close();  
				} catch( Exception e ) {}  
			}  
		}  
	}	

	public static URI getJarURI() throws URISyntaxException {
		final ProtectionDomain domain;
		final CodeSource source;
		final URL url;
		final URI uri;

		domain = MkdfaRunner.class.getProtectionDomain();
		source = domain.getCodeSource();
		url = source.getLocation();
		uri = url.toURI();

		return (uri);
	}
}
