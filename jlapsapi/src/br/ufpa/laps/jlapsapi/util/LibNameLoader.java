package br.ufpa.laps.jlapsapi.util;

//import java.io.File;

public class LibNameLoader {
	//Carrega bibliotecas nativas pro sistema operacional e arquitetura
	
	private static final String LIB_NAME = "coruja";
	
	public static String libRoot(String libFolder){

		return libFolder+"/"+libName();
	}
	
	private static String libName() throws UnsupportedOperationException {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.startsWith("windows")) {
            return libNameBare() + ".dll";

        } else if (os.startsWith("mac os x")) {
            //return "lib"+libNameBare()+".dylib";
            throw new UnsupportedOperationException("mac os x is unsupported");

        } else {
            return "lib" + libNameBare() + ".so";
        }
    }

    public static String libNameBare() throws UnsupportedOperationException {
        String os = System.getProperty("os.name").toLowerCase();
        String arch = System.getProperty("os.arch").toLowerCase();

        // Annoying that Java doesn't have consistent names for the arch types:
        boolean x86 = arch.equals("x86") || arch.equals("i386") || arch.equals("i686");
        boolean amd64 = arch.equals("x86_64") || arch.equals("amd64") || arch.equals("ia64n");

        if (os.startsWith("windows")) {
            if (x86) {
                return "hunspell-win-x86-32";
            }
            //if (amd64) {
            // Note: No bindings exist for this yet (no JNA support).
            //	return "hunspell-win-x86-64";
            //}

        } else if (os.startsWith("mac os x")) {
            if (x86) {
                return LIB_NAME + "-darwin-x86-32";
            }
            if (arch.equals("ppc")) {
                return LIB_NAME + "-darwin-ppc-32";
            }

        } else if (os.startsWith("linux")) {
            if (x86) {
                return LIB_NAME /*+ "-linux-x86-32"*/;
            }
            if (amd64) {
                return LIB_NAME /*+ "-linux-x86-64"*/;
            }

        } else if (os.startsWith("sunos")) {
            //if (arch.equals("sparc")) {
            //	return "hunspell-sunos-sparc-64";
            //}
        }

        throw new UnsupportedOperationException("Unknown OS/arch: " + os + "/" + arch);
    }

}
