package br.ufpa.laps.jlapsapi.util.conversor;
//package components;

/***
 * An interface component of TextAnalysis4BP 
 * <p>
 * 
 * Last modification in May 28, 2010
 * @author Igor Couto
 * 
**/

public interface Component {

	
    /**
     * Get the name of component.
     * @return name
     */
    public abstract String getName();
        
    /**
     * Get the component's description.
     * @return description. 
     * */
    public abstract String getDescription();
    
}
