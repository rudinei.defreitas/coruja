package br.ufpa.laps.jlapsapi.util.conversor;
//package components.g2p;

//import components.Component;
//import components.stress.StressComponent;

/**
 * This is an abstract grapheme to phoneme converter.
 * <p>
 * 
 * Last modification in May 28, 2010
 * @author Igor Couto
 * 
**/

public abstract class G2PComponent implements Component{

	/* Masculine pronouns */
	protected static String[] Prn_M = {"este","estes","esse","esses","aquele","aqueles",
		"seu","seus","teu","teus","ele","eles","dele","nele","neles","deles"};
	/* Feminine pronouns */
	protected static String[] Prn_F ={"esta","estas","essa","essas","aquela","aquelas",
		"ela","elas","dela","nela","nelas","delas"};
	/* Exceptions for sub rule 15-09  */
	protected static String[] o_exceptions = {"maior","maiores","menor","menores",
			"melhores","melhor","pior","piores","suor","suores"};
	/* A StressComponent object to support the conversion */
	protected StressComponent stress;
	/* A Enum object to stores letters */
	protected static enum Alphabet {
		a,â,ã,á,à,b,c,ç,d,e,ê,é,f,g,h,i,í,j,k,l,m,n,o,ô,ó,õ,p,q,r,s,t,u,ü,ú,v,w,x,y,z
	};
	
	/**
	 * Constructs a new G2PComponent object with a StressComponent 
	 * to support. 
	 * @param stress determines which stress determination object 
	 * will be used.
	 *
	**/
	protected G2PComponent(StressComponent stress){
		this.stress=stress;
	}
	/**
	 * Computes the algorithm that converts a grapheme 
	 * sequence for a phone sequence.
	 * @param input a grapheme sequence.
	 * @return a phoneme sequence.
	 * */
	protected abstract String process(String input);
	/**
	 * TODO: preciso arrumar para as novas letras do alfabeto k, w e y.
	 * 
	 * This method only returns true if letter is a vowel.
	 * @param letter the letter that will be analyzed.  
	 * @return true if letter is a vowel
	 * */
	protected static boolean isVowel(char letter){
		/* Set of vowels in brazilian portuguese */
		char [] vowels ={'a','e','i','o','u',
				'á','é','í','ó','ú','à','ê','â','ô','è','ã',
				'õ','y'};
		/* Search letter inside vowels */
		for(char temp:vowels){
			if (letter == temp)
				return true;
		}

		return false;
	}
	/**
	 * This method only returns true if letter is voiced consonant.
	 * @param letters the two previous or next letters that will be analyzed.  
	 * @return true if letter is a voiced consonant
	 * */
	protected boolean isVoicedConsonant(String letters){
		/* Set of voiced consonants in brazilian portuguese */
		char [] voicedConsonants={ 'b','g','d','v','z','j','n','m','r'};
				
		/* Search letter inside voicedConsonants */
		for(char temp:voicedConsonants)
			if (letters.charAt(0) == temp)
				return true;
		/* nh, ge and gi*/
		if(letters.equals("nh") || letters.equals("ge") || letters.equals("gi"))
			return true;
		
		return false;
	}
	/**
	 * This method only returns true if letter is a unvoiced consonant.
	 * @param letter the letter that will be analyzed.  
	 * @return true if letter is a unvoiced consonant
	 * */
	protected  boolean isUnvoicedConsonant(char letter){
		/* Set of unvoiced consonants in brazilian portuguese */
		char [] unvoicedConsonants ={'p','t','f','s','c'};
		/* Search letter inside unvoicedConsonants */
		for(char temp:unvoicedConsonants){
			if (letter == temp)
				return true;
		}
		return false;
	}
	/** 
	 * Only verifies if a array contains a specific string.  
	 * @param array Array with strings
	 * @param text string to search
	 * @return true if array contains text
	 */
	protected boolean hasString(String[] array, String text){
		/* Search string inside array */
		for(String temp:array){
			if(text.equals(temp))
				return true;
		}
		return false;
	}
	/**
	 * This method only returns true if the input char is a letter.
	 * @param letter the letter that will be analyzed.  
	 * @return true if the char is a letter
	 * */
	protected boolean isLetter(char letter){
		/* Set of letters in brazilian portuguese */
		char [] letters ={'a','â','ã','á','à','b','c','ç','d','e',
				'é','è','f','g','h','i','í','j','k','l','m','n','o',
				'ó','p','q','r','s','t','u','ü','ú','v','w','x',
				'y','z'};
				
		/* Search letter inside letters */
		for(char temp:letters){
			if (letter == temp)
				return true;
		}
		return false;
	}

	
}
