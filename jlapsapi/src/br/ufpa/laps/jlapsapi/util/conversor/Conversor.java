package br.ufpa.laps.jlapsapi.util.conversor;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;

public class Conversor {
	/*como o método converteparte da classe G2PRuleBased01 só converte palavras que não contenham
  espaço nos começo e também no final, esse métodos é usado para retirar esses espaços*/
	public static String RetiraEspaco(String principal){
		int indice = 0;
		if (principal.charAt(indice)==' '){
			while (principal.charAt(indice)==' '){
				principal = principal.substring(1,principal.length());
			}
			while (principal.charAt(principal.length()-1)==' '){
				principal = principal.substring(0,principal.length()-1);
			}
		}
		return principal;
	}
	public static String Converter(FileReader reader, String outfiles) throws IOException{
		//outfiles é o diretório onde os arquivos do Julius ficarão
		String grammarName="";
		String prefix ="temp";
		String temp = " ";
		String aux="";
		String aux2="";
		String sub;
		String regra="";
		String fonema="";
		BufferedReader java_grammar = new BufferedReader(reader);
		do{
			temp=java_grammar.readLine();
			aux2 = temp;
			if (temp.indexOf("grammar")>=0){
				temp=RetiraEspaco(temp);
				if (temp.indexOf(";")==temp.length()-1){
					temp=temp.substring(temp.lastIndexOf("grammar")+7, temp.indexOf(";"));
					temp=RetiraEspaco(temp);
					if (temp.indexOf(".")>=0){
						prefix = temp.substring(temp.lastIndexOf("."), temp.length());
					}
					else
						prefix = temp;
						grammarName = temp;
				}
			}
		}while(aux2.indexOf("grammar")< 0);
		BufferedWriter file_grammar = new BufferedWriter(new FileWriter(outfiles+prefix+".grammar"));
		BufferedWriter vocabulario = new BufferedWriter(new FileWriter(outfiles+prefix+".voca"));
		StressRuleBased02 stress = new StressRuleBased02();
		G2PRuleBased01 transc = new G2PRuleBased01(stress);
		int dicount = 2;
		vocabulario.write("% NS_B\n<s>\tsil\n\n% NS_E\n</s>\tsil\n");
		//Verifica se existe alguma linha comentada
		while ((temp = java_grammar.readLine())!=null){
			if (temp.equals("/**")||((temp.indexOf("/")>=0)&&(temp.indexOf("/")+1)== temp.indexOf("*"))){
				while (temp.indexOf("*/")>0){
					temp = java_grammar.readLine();
				}
			}
			// Verifica o nome da regra antes do sinal de igual e começa montar o .vocabulario
			else if(temp.indexOf("=")>0 && temp.indexOf("<")<temp.indexOf("=") && temp.indexOf(">")< temp.indexOf("=")){
				regra=temp.substring(temp.indexOf("<")+1, temp.indexOf(">"));
				file_grammar.write("S : NS_B "+regra+" NS_E\n");

				vocabulario.write("\n% "+ regra);
				vocabulario.newLine();
			}
			if (temp.indexOf("=")>=0 && temp.indexOf(";")<0){
				sub = temp+" "+java_grammar.readLine();
			}
			if (temp.indexOf("=")>=0 && temp.indexOf(";")>0){
				sub = temp.substring(temp.indexOf("=")+1,temp.indexOf(";"));
				//tratamento de regra formadas por outras regras
				/*while (sub.length()>1){
					sub = conversor.RetiraEspaco(sub);
					if (sub.indexOf("<")>=0){
						if (sub.charAt(0)=='<'){
							regra = sub.substring(1,sub.indexOf(">"));
							sub = sub.substring(sub.indexOf(">"+1),sub.length());
							//desenvolver método para procurar regra

						}
						else{
							regra = sub.substring(0, sub.indexOf("<"));
							sub = sub.substring(sub.indexOf("<"),sub.length());
						}

					}

				}*/
				if (sub.indexOf("<")>=0){
					file_grammar.write(regra+": ");
					file_grammar.newLine();
				}
				while (sub.indexOf("<")>=0){
					aux = sub.substring(sub.indexOf("<")+1,sub.indexOf(">"));
					sub = sub.substring(sub.indexOf(">"),sub.length()-1);
					file_grammar.write(" "+aux);
				}
				while (sub.indexOf("|")>=0){
					aux = sub.substring(0,sub.indexOf("|"));
					sub = sub.substring(sub.indexOf("|")+1,sub.length());
					aux = Conversor.RetiraEspaco(aux);
					StringTokenizer words = new StringTokenizer(aux);
					if (words.countTokens()>1){
						fonema ="";
						while (words.hasMoreTokens()){
							aux = words.nextToken();
							fonema = fonema +" "+ transc.converteParte(Conversor.RetiraEspaco(aux),1,aux.length());
						}
					}
					else{
						fonema = transc.converteParte(aux,1,aux.length());
						fonema = fonema.substring(0, fonema.length()-1);//tira espaço depois do fonema
					}
					vocabulario.write(aux+"\t"+fonema);
					vocabulario.newLine();					
				}
				aux=sub.substring(1,sub.length());
				aux = Conversor.RetiraEspaco(aux);
				fonema = transc.converteParte(aux,1,aux.length());
				fonema = fonema.substring(0,fonema.length()-1);//tira o espaço em branco do final do fonema
				vocabulario.write(aux+"\t"+fonema+"\n");
				dicount++;
			}
		}
		file_grammar.close();
		vocabulario.close();
		return grammarName;
	}
	/*public static void main (String[] args) throws IOException{
	Conversor conv = new Conversor();
	conv.Converter("/home/20102000011/Documents/teste.grammar");
}*/
	/*public static void main(String[] args){
		//String path = "/home/20102000011/Documents/teste.grammar";
		String path = "/home/10080000701/workspace/jlapsapi/dirgram/teste.grammar";
		String resultado []= new String[2];
		if (path!=null){
		try {
			//resultado = Converter (args[0]);
			resultado = Converter (path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
		else
			System.out.println("Falta o argumento");
}*/
}