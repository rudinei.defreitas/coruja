package br.ufpa.laps.jlapsapi.util.conversor;
//package components.syll;

import java.util.LinkedList;

//import components.stress.StressRuleBased02;

public class SyllabificationRuleBased01 extends SyllabificationComponent{

	private static String[] palavra;
	private String saida;
	private int comprimento;
	private int nsilaba;
	private static int[] silIndices;
	private boolean debugMode=true;
	private String delimitador="-";
	StressRuleBased02 stress = new StressRuleBased02();
	/* Índice que armazena a posição onde o "resto" da palavra começa
	(Entenda "resto" como a porção da palavra que ainda não foi dividida silabicamente)*/
	private int indResto;
	
	@Override
	public String process(String entrada) {
		// TODO Auto-generated method stub
		
		//Recuperação do índice da vogal tônica (començando com índice = 0)
		int indStress = stress.process(entrada) - 1;
		
		/* String de retorno*/
		saida="";
		
		if(entrada.equals("bueiro")) 
			System.out.print(""); 
		
		/* Converte a simples String em um array de Strings. Cada elemento 
		 * do array corresponde a uma letra. */
		palavra = convertString2ArrayString(entrada);
		
		// Aponta para a primeira letra da palavra
		indResto = 0;
		
		/* Armazena a quantidade de letras. */
		comprimento=palavra.length;
		/* Iniciliza o contador de sílabas. */
		nsilaba=1;		
		/* Inicializa  seta para zero todos os valores de [silIndices]. 
		 * -1 significa sem sílaba*/	
		silIndices = new int[comprimento];
		for (int x = 0; x < comprimento; x++) 
			silIndices[x]=-1;
		
		for (int ind = 0; ind < comprimento; ind++) {

			/* Inicializa todos as variáveis. */
			@SuppressWarnings("unused")
			String aaaletra="#",aaletra="#",aletra="#",
			letra="#",pletra="#",ppletra="#",pppletra="#";
			/* Inicializa as variáveis, evitando ArrayOutOfBoundsExceptions. */
			switch (ind) {
			default: /* Em casos que o [ind] é maior que 3. */
			case 3: aaaletra=palavra[ind-3];
			case 2:  aaletra=palavra[ind-2];
			case 1:	  aletra=palavra[ind-1];
			case 0:
				break;
			}
			/* Inicializa as variáveis, evitando ArrayOutOfBoundsExceptions. */
			/* [comprimento] sempre começa em 1. E [ind] em zero, fazendo o 
			 * caso 1 ser sempre verdadeiro. */
			switch (comprimento-ind) {
			default: /* Em casos que o [comprimento-ind] é maior que 4. */
			case 4:pppletra=palavra[ind+3];
			case 3: ppletra=palavra[ind+2];
			case 2:	 pletra=palavra[ind+1];
			case 1:	  letra=palavra[ind];
				break;
			}	
		
			delimitador="-";
			
			//Regra Extra (Ditongos Decrescentes)
			if (! ((aletra.equals("u")) || (aletra.equals("ü")) || (aletra.equals("gu")) || 
				   (aletra.equals("gü")) || (aletra.equals("qu")) || (aletra.equals("qü"))))
			{
				if(((letra.equals("a")) || (letra.equals("á")) || (letra.equals("ã")) || 
					(letra.equals("â")) || (letra.equals("à")) || (letra.equals("e")) || 
					(letra.equals("é")) || (letra.equals("ê")) || (letra.equals("o")) || 
					(letra.equals("ó")) || (letra.equals("õ")) || (letra.equals("ô")))
					&& 
				   ((pletra.equals("i")) || (pletra.equals("í")) || (pletra.equals("u")) || 
					(pletra.equals("ú")) || (pletra.equals("ü")))) 
					{
						if (indStress > -1) {
							if (Character.toString(entrada.charAt(indStress)).equals(pletra)) {
								if(debugMode) System.out.print("Regra Ditongo Desc | ");
								if(saida.equals("")) delimitador="";
								saida=saida+delimitador+Caso03(ind);
								continue;
							}
							else {
								if(debugMode) System.out.print("Regra Ditongo Desc | ");
								if(saida.equals("")) delimitador="";
								if((ppletra.equals("s")) && isConsonant(ind+3)) {
									saida=saida+delimitador+Caso05(ind);
									ind=ind+2;
									continue;
								}
								else {
									saida=saida+delimitador+Caso04(ind);
									ind++;
									continue;
								}
							}
						}
					}
			}
			
			//Regra Extra (Ditongos Que Variam Com Hiato)
			if (!(((letra.equals("u")) || (letra.equals("ü")) || (letra.equals("gu")) || 
				   (letra.equals("gü")) || (letra.equals("qu")) || (letra.equals("qü"))) 
				  && 
				   ((pletra.equals("a")) || (pletra.equals("ã")) || (pletra.equals("e")) || 
					(pletra.equals("i")) || (pletra.equals("o")) || (pletra.equals("õ"))) 
				  && 
				   ((ppletra.equals("e")) || (ppletra.equals("i")) || 
					(ppletra.equals("o")) || (ppletra.equals("u")))))
			{
				if(((letra.equals("i")) || (letra.equals("í")) || (letra.equals("u")) || 
					(letra.equals("ú")) || (letra.equals("ü")))
					&& 
					((pletra.equals("a")) || (pletra.equals("á")) || (pletra.equals("ã")) || 
					 (pletra.equals("â")) || (pletra.equals("à")) || (pletra.equals("e")) || 
					 (pletra.equals("é")) || (pletra.equals("ê")) || (pletra.equals("o")) || 
					 (pletra.equals("ó")) || (pletra.equals("õ")) || (pletra.equals("ô")))) 
					{
						if (!(ppletra.equals("#")) && !(((ppletra.equals("s"))) && ((pppletra.equals("#"))))) {
							if(debugMode) System.out.print("Regra Ditongo Hiato | ");
							if(saida.equals("")) delimitador="";
							saida=saida+delimitador+Caso03(ind);
							continue;
						}
						else {
							if (indStress > -1) {
								if((Character.toString(entrada.charAt(indStress)).equals(letra)) ||
								   (Character.toString(entrada.charAt(indStress)).equals(pletra))) 
								{
									if(debugMode) System.out.print("Regra Ditongo Hiato | ");
									if(saida.equals("")) delimitador="";
									saida=saida+delimitador+Caso03(ind);
									continue;
								}
								else {
									if(debugMode) System.out.print("Regra Ditongo Hiato | ");
									if(saida.equals("")) delimitador="";
									saida=saida+delimitador+Caso06(ind);
									break;
								}
							}
						}
					}
			}
			
			//Regras do Paper
			if(isVowel(ind)){
				if(ind==indResto){
					System.out.println("Analisou 1 a 5...............");
					/* Regra 01 *****************/
					/* V=p0 and V!=<ã>,<õ> and { {^(+1)=V and ^(+1)!=G}  or {^(+1)!=G and ^(+2)=<n>}}*/
					if( (!letra.equals("ã")&&!letra.equals("õ")) && ((isVowel(ind+1)&&!isSemivowel(ind+1))||(isSemivowel(ind+1)&&ppletra.equals("n"))) ) {
						if(debugMode) System.out.print("Regra 01 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso01(ind);
						continue;
					}
					/* Regra 02 *****************/
					/* V=p0 and ^(+1)=C and ^(+2)!=C and ^(+3)=CO */
					if( isConsonant(ind+1) && isConsonant(ind+2) && isOclusive(ind+3) )	{
						if(debugMode) System.out.print("Regra 02 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso01(ind);
						ind=ind+2; // O caso 5 faz a vogal agregar as 2 consoantes seguintes.   
						continue;
					}
					/* Regra 03 *****************/
					/* V=p0 and ^(+1)=G,<s>,<r>,<l>,CN,<x> and ^(+2)=C and ^(+2)!=<s><h><r> */
					if( (isSemivowel(ind+1)||pletra.equals("s")||pletra.equals("r")||pletra.equals("l")||isNasal(ind+1)||pletra.equals("x")) &&
						(isConsonant(ind+2)) &&
						(!ppletra.equals("s")||!ppletra.equals("h")||!ppletra.equals("r")) &&
						!pletra.equals(ppletra) && !ppletra.equals("h"))
					{
						if(debugMode) System.out.print("Regra 03 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso02(ind);
						ind++;
						continue;
					}
					/* Regra 04 *****************/
					/* V=p0 and ^(+1)=C and ^(+2)=C and ^(+3)=V */
					if( isConsonant(ind+1) && isConsonant(ind+2) && isVowel(ind+3)) {
						if(debugMode) System.out.print("Regra 04 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso01(ind);
						continue;
					}
					/* Regra 05 *****************/
					/* V=p0 and ^(+1)=C and ^(+2)=V,CL */
					if( isConsonant(ind+1) && (isVowel(ind+2)||isLiquids(ind+2))) {
						if(debugMode) System.out.print("Regra 05 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso01(ind);
						continue;
					}
					
				}else {
					System.out.println("Analisou 6 a 20...............");
					/* Regra 06 *****************/
					/* V!=p0 and ^(-1)=C and ^(+1)=C and ^(+2)=V */
					if( isConsonant(ind-1) && isConsonant(ind+1) &&	isVowel(ind+2))	{
						if(debugMode) System.out.print("Regra 06 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso03(ind);
						continue;
					}
					/* Regra 07 *****************/
					/* V!=p0 and ^(-1)=C,G and ^(+1)=G and ^(+2)=C and ^(+2)!=<m>,<n>,<r>,<s> and ^(+3)=SP,F,C */
					if( (isConsonant(ind-1)||isSemivowel(ind-1)) &&	
						isSemivowel(ind+1) &&
						(isConsonant(ind+2) && 
							!(ppletra.equals("m")&&pppletra.equals("#")||isConsonant(ind+3)) && 
							!(ppletra.equals("n")&&pppletra.equals("#")||isConsonant(ind+3)) &&
							!(ppletra.equals("r")&&pppletra.equals("#")||isConsonant(ind+3)) &&
							!(ppletra.equals("s")&&pppletra.equals("#")||isConsonant(ind+3))) ) 
					{
						if(debugMode) System.out.print("Regra 07 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso04(ind);
						ind++;
						continue;
					}
					/* Regra 08 *****************/
					/* V!=p0 and ^(-1)=C and ^(+1)=G and ^(+2)=<s> and ^(+3)=CO */
					if( (isConsonant(ind-1)) &&	isSemivowel(ind+1) && ppletra.equals("s") && isOclusive(ind+3)) {
						if(debugMode) System.out.print("Regra 08 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso05(ind);
						ind=ind+2;
						continue;
					}
					/* Regra 09 *****************/
					/* V!=p0 and ^(-1)=C and ^(+1)=G and ^(+2)=V,SP */
					if( (isConsonant(ind-1)) &&	(isSemivowel(ind+1) /*|| pletra.equals("o")*/) && (isVowel(ind+2)||ppletra.equals("#"))) {
						if(debugMode) System.out.print("Regra 09 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso04(ind);
						ind++;
						continue;
					}
					/* Regra 10 *****************/
					/* V!=p0 and ^(-1)=G and ^(+1)=C and ^(+2)=V */
					if( (isSemivowel(ind-1)) && isConsonant(ind+1) && isVowel(ind+2))	{
						if(debugMode) System.out.print("Regra 10 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso03(ind);
						continue;
					}
					/* Regra 11 *****************/
					/* V!=p0 and ^(-1)=C and ^(+1)=G and ^(+2)=<r> and ^(+3)=C */
					if( isConsonant(ind-1) && isSemivowel(ind+1) && ppletra.equals("r") &&	isConsonant(ind+3)) {
						if(debugMode) System.out.print("Regra 11 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso03(ind);
						continue;
					}
					/* Regra 12 *****************/
					/* V!=p0 and ^(-1)=C,{<u>,<ü> antecedidos de <q>,<g>} and ^(+1)=CL,CN,<c>,<x> and ^(+3)=V,<h>,<l>,<r> */
					if( ( isConsonant(ind-1) || ((aletra.equals("u")||aletra.equals("ü"))&&(aaletra.equals("q")||aaletra.equals("g")))) &&
						( isLiquids(ind+1)||isNasal(ind+1)||pletra.equals("c")||pletra.equals("x") ) &&
						( isVowel(ind+3)||pppletra.equals("h")||pppletra.equals("l")||pppletra.equals("r")) )
					{
						if(debugMode) System.out.print("Regra 12 | ");
						if(saida.equals("")) delimitador="";
						if(ppletra.equals("h")||ppletra.equals("l")||ppletra.equals("r"))
							saida=saida+delimitador+Caso01(ind);
						else {	
							saida=saida+delimitador+Caso04(ind);
							ind++;
						}		
						continue;
					}
					/* Regra 13 *****************/
					/* V!=p0 and ^(-1)=C and ^(+1)=CL,CN,<i> */
 					if( ( isConsonant(ind-1)) && ( isLiquids(ind+1) || isNasal(ind+1)||pletra.equals("i"))) 
					{
						if(debugMode) System.out.print("Regra 13 | ");
						if(saida.equals("")) delimitador="";
						if(ppletra.equals("#")){
							saida=saida+delimitador+Caso06(ind);
							break;
						}
						else {
							if(ppletra.equals("s")){
								saida=saida+delimitador+Caso05(ind);
								ind=ind+2;
								continue;
							}
							/*else {
								saida=saida+delimitador+Caso02(ind);
								ind++;
								continue;
							}*/
						}	
					}
					/* Regra 14 *****************/
					/* V!=p0 and ^(+1)=V igual or ^(+1)=V and ^(+2)=V */
					if(  (letra.equals(pletra)) || (isVowel(ind+1) && isVowel(ind+2))  ) {
						if(debugMode) System.out.print("Regra 14 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso01(ind);
						continue;
					}
					/* Regra 15 *****************/
					/* V!=p0 and ^(+1)=CO,<f>,<v>,<g> and ^(+2)=CL,CO and ^(+3)=V */
					if(	( isOclusive(ind+1)||pletra.equals("p")||pletra.equals("v")||pletra.equals("g")) &&
						( isLiquids(ind+2)||isOclusive(ind+2) ) &&
						( isVowel(ind+3) ) ) 
					{
						if(debugMode) System.out.print("Regra 15 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso01(ind);
						continue;
					}
					/* Regra 16 *****************/
					/* V!=p0 and V=<i> ^(-1)=C and ^(+1)=<a>,<o> and ^(+2)=F */
					if( letra.equals("i") && isConsonant(ind-1) &&	( pletra.equals("a")||pletra.equals("o") ) && ppletra.equals("#")) {
						if(debugMode) System.out.print("Regra 16 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso03(ind);
						continue;
					}
					/* Regra 17 *****************/
					/* V!=p0 and V=<ã>,<õ> ^(-1)=C and ^(+1)=<o>,<e> and ^(+2)=SP,F,<s> */
					if( (letra.equals("ã")||letra.equals("õ")) && (isConsonant(ind-1)) &&	(pletra.equals("o")||pletra.equals("e")) && (ppletra.equals("#")||ppletra.equals("s")) ) {
						if(debugMode) System.out.print("Regra 17 | ");
						if(saida.equals("")) delimitador="";
						saida=saida+delimitador+Caso06(ind);
						break;
					}
					/* Regra 18 *****************/
					/* V!=p0 and ^(-1)=C,<u> antecedido de <q> and ^(+1)=C and ^(+2)=C */
					if( (isConsonant(ind-1)||(aletra.equals("u")&&aaletra.equals("q"))) && isConsonant(ind+1) && isConsonant(ind+2)) {
						if(debugMode) System.out.print("Regra 18 | ");
						if(saida.equals("")) delimitador="";
						if(pletra==ppletra){
							saida=saida+delimitador+Caso01(ind);
						}
						if(pletra.equals("s") && !ppletra.equals("s")){
							saida=saida+delimitador+Caso02(ind);
							ind++;
						}
						if(ppletra.equals("s") && isOclusive(ind+3)){
							saida=saida+delimitador+Caso05(ind);
							ind=ind+2;
						}
						continue;
					}
					/* Regra 19 *****************/
					/* V!=p0 and ^(+1)=V and ^(+2)=C */
					if( isVowel(ind+1) && isConsonant(ind+2)) {
						if(debugMode) System.out.print("Regra 19 | ");
						if(saida.equals("")) delimitador="";
						/*if(isVowel(ind+3)) && (!pletra.equals("í") && !pletra.equals("ú"))){
							saida=saida+delimitador+Caso02(ind);
							ind++;
						}else{
							saida=saida+delimitador+Caso01(ind);
						}*/
						saida=saida+delimitador+Caso01(ind);
						continue;
					}
					/* Regra 20.a */
					if((pletra.equals("n")||pletra.equals("r")||pletra.equals("l")) && (!isVowel(ind+2)&&!ppletra.equals("h"))){
						if(debugMode) System.out.print("Regra 20.a| ");
						if(saida.equals("")) delimitador="";
						if(ppletra.equals("s")&&isVowel(ind+3)){
							saida=saida+delimitador+Caso05(ind);
							ind=ind+2;
						}else {
							saida=saida+delimitador+Caso02(ind);
							ind++;
						}
						continue;
					}
					/* Regra 20.b */
					/* Varre na palavra em busca de vogais */
					boolean exe=false;
					for (int j = ind+1; j < comprimento; j++) {
						if(isVowel(j)){
							if(debugMode) System.out.print("Regra 20.b| ");
							if(saida.equals("")) delimitador="";
							saida=saida+delimitador+Caso01(ind);
							exe=true;
							break;
						}
						continue;
					}
					if(exe) continue;
					
				}
				if(debugMode) System.out.print("Regra 20.c| ");
				if(saida.equals("")) delimitador="";
				saida=saida+delimitador+excecao(ind);
				break;
				
			}
			
						
			
			
		}
		
		
		return saida;
	}
	private String Caso01(int ind){
		if(debugMode) System.out.println("Caso01");
		String temp="";
		for (int i = 0; i <= ind; i++)
			if(silIndices[i]==-1)
				temp = temp+palavra[i];
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i <= ind; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		// Aponta para a primeira letra da porção não dividida da palavra
		indResto = ind + 1;
		
		return temp;
	}
	private String Caso02(int ind){
		if(debugMode)System.out.println("Caso02");
		String temp="";
		for (int i = 0; i <= ind+1; i++) 
			if(silIndices[i]==-1)
				temp = temp+palavra[i];
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i <= ind+1; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		// Aponta para a primeira letra da porção não dividida da palavra
		indResto = ind + 2;
		
		return temp;
	}
	private String Caso03(int ind){
		if(debugMode)System.out.println("Caso03");
		String temp="";
		for (int i = 0; i <= ind; i++) 
			if(silIndices[i]==-1)
				temp = temp+palavra[i];
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i <= ind; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		// Aponta para a primeira letra da porção não dividida da palavra
		indResto = ind + 1;
		
		return temp;
	}
	private String Caso04(int ind){
		if(debugMode)System.out.println("Caso04");
		String temp="";
		for (int i = 0; i <= ind+1; i++) 
			if(silIndices[i]==-1)
				temp = temp+""+palavra[i];
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i <= ind+1; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		// Aponta para a primeira letra da porção não dividida da palavra
		indResto = ind + 2;
		
		return temp;
	}
	private String Caso05(int ind){
		if(debugMode)System.out.println("Caso05");
		String temp="";
		for (int i = 0; i <= ind+2; i++) 
			if(silIndices[i]==-1)
				temp = temp+""+palavra[i];
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i <= ind+2; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		// Aponta para a primeira letra da porção não dividida da palavra
		indResto = ind + 3;
		
		return temp;
	}
	private String Caso06(int ind){
		if(debugMode)System.out.println("Caso06");
		String temp="";
		for (int i = 0; i < comprimento; i++) 
			if(silIndices[i]==-1)
				temp = temp+""+palavra[i];
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i < comprimento; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		return temp;
	}
	@SuppressWarnings("static-access")
	private String excecao(int ind){
		if(debugMode)System.out.println("excecao");
		String temp="";
		String palavra;
		for (int i = 0; i < comprimento; i++){
			palavra=this.palavra[i];	
			if(silIndices[i]==-1)
				temp = temp+""+palavra;
		}
		/* Cada elemento de [silIndices] indica a qual sílaba a respectiva
		 * letra representa. */
		for (int i = 0; i < comprimento; i++)
			if(silIndices[i]==-1)
				silIndices[i]=nsilaba;
		/* Incrementa para a próxima sílaba. */
		nsilaba++;
		
		return temp;
	}
	private String[] convertString2ArrayString(String entrada){
		
		String[] palavraTemp;
		/* Terão o mesmo tamanho. */
		//LinkedList<Character> sim_temp = new LinkedList<Character>();
		LinkedList<String> graf_temp = new LinkedList<String>(); 
		@SuppressWarnings("unused")
		char a_graf;
		char graf; 
		char p_graf;
		char pp_graf;
		
		for (int i = 0; i < entrada.length(); i++) {
			/* Carrega a letra atual e as duas próximas, se existirem. */
			a_graf='#';
			graf=entrada.charAt(i);
			p_graf='#';
			pp_graf='#';
			if(i > 0)
				a_graf=entrada.charAt(i-1);
			if(i < entrada.length()-1)
				p_graf=entrada.charAt(i+1);
			if(i < entrada.length()-2)
				pp_graf=entrada.charAt(i+2);
			
			/* Analisa o grafema, rotulando os simbolos e armazenando os grafemas. */
			if(graf=='p'||graf=='t'||graf=='b'||graf=='d'){
				/* Grafema com uma letra */
		//		sim_temp.add(oclusiva);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			if(graf=='f'||graf=='v'||graf=='z'||graf=='j'||graf=='x'||graf=='ç'){
				/* Grafema com uma letra */
		//		sim_temp.add(fricativa);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			if(graf=='m'){
				/* Grafema com uma letra */
		//		sim_temp.add(nasal);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			if(graf=='l'){
				if(p_graf=='h'){
					/* Grafemas com duas letras - lh */
		//			sim_temp.add(consoante);
					graf_temp.add(entrada.substring(i,i+2));
					i++;
					continue;
				}
				else{
					/* Grafemas com uma letra - l */
		//			sim_temp.add(liquida);
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
			}
			if(graf=='r'){
		//		sim_temp.add(liquida);
				if(p_graf=='r'){
					/* Grafemas com duas letras - rr */
					graf_temp.add(entrada.substring(i,i+2));
					i++;
					continue;
				}else{
					/* Grafemas com uma letra - r */
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
			}
			if(graf=='c'){
				if(p_graf=='e'||p_graf=='i'||p_graf=='h'){
		//			sim_temp.add(fricativa);
					if(p_graf=='h'){
						/* Grafemas com duas letras - ch */
						graf_temp.add(entrada.substring(i,i+2));
						i++;
						continue;
					}
					else{
						/* Grafemas com uma letra - ce  ci */
						graf_temp.add(entrada.substring(i,i+1));
						continue;
					}
				}
				else {
					/* Grafemas com uma letra - ca co  kodac  */
		//			sim_temp.add(oclusiva);
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
			}
			if(graf=='s'){
		//		sim_temp.add(fricativa);
				if(p_graf=='s'){
					/* Grafemas com duas letras - assar */
					graf_temp.add(entrada.substring(i,i+2));
					i++;
					continue;
				}else{
					/* Grafemas com uma letra - sapato  samba */
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
			}
			if(graf=='n'){
				if(p_graf=='h'){
					/* Grafemas com duas letras - ninho */
			//		sim_temp.add(consoante);
					graf_temp.add(entrada.substring(i,i+2));
					i++;
					continue;
				}else {
					/* Grafemas com uma letra - nome nada */
			//		sim_temp.add(nasal);
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
			}
			if(graf=='q'){
				if((p_graf=='u'||p_graf=='ü')){
					/* Grafemas com duas letras - qu qü */
					graf_temp.add(entrada.substring(i,i+2));
					i++;
					if(pp_graf=='e'||pp_graf=='i'){
						/* quimera quem quinto aqui quente */
					//	sim_temp.add(oclusiva);
					}
					else {
						/* quanto quota quartzo */
				//		sim_temp.add(consoante);
					}
					continue;
				}else {
					/* Grafemas com uma letra - ???  */
				//	sim_temp.add(consoante);
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
			}
			if(graf=='g'){
				if(p_graf=='u'||p_graf=='ü'){
					/* Grafemas com duas letras - gu gü*/
					graf_temp.add(entrada.substring(i,i+2));
					i++;
					if(pp_graf=='e'||pp_graf=='i'){
						/* guerra  guindaste  guia */
				//		sim_temp.add(oclusiva);
					}else {
						/* guanabara   */
				//		sim_temp.add(consoante);			
					}
					continue;
				}
				if(p_graf=='a'||p_graf=='o'||p_graf=='u'){
					/* garoto  gordo  gula  */
			//		sim_temp.add(oclusiva);
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
				if(p_graf=='e'||p_graf=='i'){
					/* gelo  gilete   */					
		//			sim_temp.add(fricativa);
					graf_temp.add(entrada.substring(i,i+1));
					continue;
				}
				/* Nenhum caso anterior      ???? */
		//		sim_temp.add(consoante);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			if(graf=='h'){
				/* hoje hora horror */
	//			sim_temp.add(consoante);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			if(graf=='k'){
				/* kia */
		//		sim_temp.add(consoante);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			if(graf=='w'){
				/* watt */
		//		sim_temp.add(consoante);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			/* Vogais, considero o Y uma vogal */
			if(isVowel(graf+"")){
			//	sim_temp.add(vogal);
				graf_temp.add(entrada.substring(i,i+1));
				continue;
			}
			/*  */
		//	sim_temp.add('#');
			graf_temp.add(entrada.substring(i,i+1));
		}
		
		int i=0;
		palavraTemp=new String[graf_temp.size()];
		for (String g : graf_temp) {
			palavraTemp[i]=g;
			i++;
		}
		
		return palavraTemp;
		
	}
	protected boolean isVowel(String letra){
			
		/* Set of vowels in brazilian portuguese */
		String [] vogais ={"a","e","i","u","o","á","é","í","ó",
			"ú","ã","õ","â","ê","ô","à","ü","y"};
	
		for(String temp:vogais)
			if (letra.equals(temp))
				return true;
	
		return false;
	}
	protected boolean isVowel(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		/* Vogais do português brasileiro */
		String [] vogais ={"a","e","i","u","o","á","é","í","ó",
			"ú","ã","õ","â","ê","ô","à","ü","y"};
	
		String letra = palavra[ind];
		for(String temp:vogais)
			if (letra.equals(temp))
				return true;
	
		return false;
	}
	protected boolean isSemivowel(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		/* Semivogais do português brasileiro */
		String [] semivowels ={"i","u"};
		/*  */
		String letra = palavra[ind];
		for(String temp:semivowels)
			if (letra.equals(temp))
				return true;
		
		return false;		
	}
	protected boolean isConsonant(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		/* Set of vowels in brazilian portuguese */
		String [] vogais ={"b","c","d","f","g","h","j","k","l",
				"m","n","p","q","r","s","t","v","x","z","w","gu","qu"};
				
		/* Inicializa as variáveis com os carateres auxiliares. */
		@SuppressWarnings("unused")
		String letra="#",pletra="#",ppletra="#",pppletra="#";
		/* A estrutura tem como função inicializar corretamente as variáveis.
		 * Evitando ArrayOutOfBoundsException. */
		/* [valor] é a quantidade de letras que ainda existe até o final de palavra.
		 * [comprimento] é subtraído de 1, porque ind sempre começa em 0.  */
		int valor=(comprimento-1)-ind;
		switch (valor) {
			default:
			case 3:pppletra=palavra[ind+3];	
			case 2: ppletra=palavra[ind+2]; 
			case 1:  pletra=palavra[ind+1];
			case 0:   letra=palavra[ind];
				break;
			case-1: //System.err.println("ERRO:"+palavra+" com index inválido.");
			case-2:
			case-3:
			case-4:
				break;
		}
		
		for(String temp:vogais)
			if (letra.equals(temp))
				return true;
		
		if(letra.equals("lh") || letra.equals("nh"))
			return true;	
		
		if(isOclusive(ind))
			return true;
		
		if(isFricative(ind))
			return true;

		if(isLiquids(ind))
			return true;
		
		if(isNasal(ind))
			return true;

		return false;
	}
	protected boolean isOclusive(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		/* Oclusiva do português brasileiro */
		String[] oclusivas_01 ={"p","t","b","d"};

		/* Inicializa as variáveis com os carateres auxiliares. */
		@SuppressWarnings("unused")
		String letra="#",pletra="#",ppletra="#",pppletra="#";
		/* A estrutura tem como função inicializar corretamente as variáveis.
		 * Evitando ArrayOutOfBoundsException. */
		/* [valor] é a quantidade de letras que ainda existe até o final de palavra.
		 * [comprimento] é subtraído de 1, porque ind sempre começa em 0.  */
		int valor=(comprimento-1)-ind;
		switch (valor) {
			default:
			case 3:pppletra=palavra[ind+3];	
			case 2: ppletra=palavra[ind+2]; 
			case 1:  pletra=palavra[ind+1];
			case 0:   letra=palavra[ind];
				break;
			case-1: //System.err.println("ERRO:"+palavra+" com index inválido.");
			case-2:
			case-3:
			case-4:
				break;
		}
		
		/* p t b d */
		for (String c : oclusivas_01) 
			if(letra.equals(c))
				return true;
		/* c+(e,i) */
		if(letra.equals("c")&&(pletra.equals("a")||pletra.equals("o")||pletra.equals("u")))
			return true;
		/* g+(e,i) */
		if(letra.equals("g")&&(pletra.equals("a")||pletra.equals("o")||pletra.equals("u")))
			return true;
		/* gu+(e,i) */
		if(letra.equals("gu")&&(pletra.equals("e")||pletra.equals("i")))
			return true;
		/* qu+(e,i) */
		if(letra.equals("qu")&&(pletra.equals("e")||pletra.equals("i")))
			return true;
		
		return false;
	}
	protected boolean isFricative(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		/* Consoantes fricativas no português brasileiro */
		String[] fricativas_01 ={"f","v","s","ç","z","j","x"};
		//char [] fricativas_02 ={'c','s','g'};
				
		/* Inicializa as variáveis com os carateres auxiliares. */
		@SuppressWarnings("unused")
		String letra="#",pletra="#",ppletra="#",pppletra="#";
		/* A estrutura tem como função inicializar corretamente as variáveis.
		 * Evitando ArrayOutOfBoundsException. */
		/* [valor] é a quantidade de letras que ainda existe até o final de palavra.
		 * [comprimento] é subtraído de 1, porque ind sempre começa em 0.  */
		int valor=(comprimento-1)-ind;
		switch (valor) {
			default:
			case 3:pppletra=palavra[ind+3];	
			case 2: ppletra=palavra[ind+2]; 
			case 1:  pletra=palavra[ind+1];
			case 0:   letra=palavra[ind];
				break;
			case-1: //System.err.println("ERRO:"+palavra+" com index inválido.");
			case-2:
			case-3:
			case-4:
				break;
		}
		
		/* f v s ç z j x */
		for (String c : fricativas_01) 
			if(letra.equals(c))
				return true;
		/* ch */
		if(letra.equals("ch"))
			return true;
		/* c+e,i */
		if(letra.equals("c")&&(pletra.equals("e")||pletra.equals("i")))
			return true;
		/* ss */
		if(letra.equals("ss"))
			return true;
		/* g+(e,i) */
		if(letra.equals("g")&&(pletra.equals("e")||pletra.equals("i")))
			return true;
		
		return false;
	}
	protected boolean isLiquids(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		String letra = palavra[ind];
		
		/* r */
		if(letra.equals("r"))
			return true;
		/* rr */
		if(letra.equals("rr"))
			return true;
		/* l except lh */
		if(letra.equals("l"))
			return true;
		
		return false;
	}
	protected boolean isNasal(int ind){
		/* Verifica se o índice é inválido. */
		if(comprimento-ind<1)
			return false;
		
		String letra = palavra[ind];
		
		String [] nasais ={"m","n"};
		
		for(String temp:nasais)
			if (letra.equals(temp))
				return true;
		
		return false;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
//	private String[][] separaHifen(String palavra){
//		
//		/* First of all, verifies if the grapheme has hyphen*/
//		/* String that will be process */
//		String [][] pSeparada = new String[1][palavra.length()];
//				
//		/* Rule of hyphen - Words with hyphen */
//		Pattern p = Pattern.compile("-");
//		Matcher m = p.matcher(palavra);
//		if(m.find()){
//			/* Splits word */
//			String[] partes = palavra.split("-");
//			/* If word has two parts: guarda-chuva / splittedGrapheme = #guarda-chuva#*/
//			if(partes.length==2){
//				pSeparada = new String[1][palavra.length()];
//				pSeparada[0] = partes[0]; /* splittedGrapheme[0]    = ##guarda##*/ 
//				pSeparada[1] = partes[1];    /* splittedGrapheme[1] = ##chuva## */
//			}
//			/* If word has tree parts: chapeu-de-sol */
//			if(partes.length==3){
//				pSeparada = new String[3];
//				pSeparada[0] = partes[0];   /* splittedGrapheme[0]    = ##chapeu##*/
//				pSeparada[1] = partes[1];  /* splittedGrapheme[1] = ##de## */
//				pSeparada[2] = partes[2];      /* splittedGrapheme[2] = ##sol## */
//			}
//			/* Exist? */
//			if(partes.length>3){
//				System.err.println("BAD WORD FORMAT: "+ pSeparada[0]);
//				return null;
//			}
//		}else {
//			/* No hyphen word*/
//			pSeparada[0][0]=palavra;
//		}
//		
//		return pSeparada;
//	}

	
	
	
	
		
}

