package br.ufpa.laps.jlapsapi.jsapi;

import java.util.Locale;

import javax.speech.Engine;
import javax.speech.EngineCreate;
import javax.speech.EngineException;
import javax.speech.recognition.RecognizerModeDesc;
	
public class JLaPSAPIRecognizerModeDesc extends RecognizerModeDesc implements
		EngineCreate {
	
	public JLaPSAPIRecognizerModeDesc() {

		super("Coruja",
				"general",
				new Locale("pt","BR"), 
				Boolean.FALSE, //Running
				Boolean.FALSE, //dictationGrammarSupported
				null);		   //speakerProfile
	}

	@Override
	public Engine createEngine() throws IllegalArgumentException,
			EngineException, SecurityException {
		return new JLaPSAPIRecognizer(this);
	}
	
	public Boolean isDictationGrammarSupported(){
		return true;
		
	}
	
}
