package br.ufpa.laps.jlapsapi.jsapi;

import java.io.IOException;

import javax.speech.recognition.RuleGrammar;

public class JSGFGrammar {
	
	private RuleGrammar ruleGrammar;
	
	public JSGFGrammar() {
		System.out.println("[JSGFGrammar] : JSGFGrammar()");
	}

	public RuleGrammar getRuleGrammar() {
		return ruleGrammar;
	}
	
    public void commitChanges() throws IOException {
        System.out.println("[JSGFGrammar] : commitChanges");
    }

}
