package br.ufpa.laps.jlapsapi.jsapi;

import javax.speech.EngineCentral;
import javax.speech.EngineList;
import javax.speech.EngineModeDesc;

public class JLaPSAPIEngineCentral implements EngineCentral {

	static private JLaPSAPIRecognizerModeDesc jjuliosModeDesc = new JLaPSAPIRecognizerModeDesc();

	@SuppressWarnings("unchecked")
	public EngineList createEngineList(EngineModeDesc engineModeDesc)
			throws SecurityException {
		
		if (engineModeDesc == null || jjuliosModeDesc.match(engineModeDesc)) {
			EngineList engineList = new EngineList();
			engineList.addElement(jjuliosModeDesc);
			return engineList;
		}
		return null;
	}

}
