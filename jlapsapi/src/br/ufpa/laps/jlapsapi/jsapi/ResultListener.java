package br.ufpa.laps.jlapsapi.jsapi;

import java.util.EventListener;

import br.ufpa.laps.jlapsapi.recognizer.Result;

public interface ResultListener extends EventListener{
	
	public void resultAccepted(Result result);
}
