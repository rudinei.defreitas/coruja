﻿namespace pptBR
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.enRec = new System.Windows.Forms.Button();
            this.disRec = new System.Windows.Forms.Button();
            this.openPPT = new System.Windows.Forms.Button();
            this.recogetionRichTextBox1 = new System.Windows.Forms.RichTextBox();
            this.openFileDialog_ppt = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.jconfTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openFileDialogJConf = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // enRec
            // 
            this.enRec.Enabled = false;
            this.enRec.Location = new System.Drawing.Point(48, 55);
            this.enRec.Name = "enRec";
            this.enRec.Size = new System.Drawing.Size(146, 23);
            this.enRec.TabIndex = 0;
            this.enRec.Text = "Habilitar Reconhecedor";
            this.enRec.UseVisualStyleBackColor = true;
            this.enRec.Click += new System.EventHandler(this.enRec_Click);
            // 
            // disRec
            // 
            this.disRec.Enabled = false;
            this.disRec.Location = new System.Drawing.Point(48, 84);
            this.disRec.Name = "disRec";
            this.disRec.Size = new System.Drawing.Size(146, 23);
            this.disRec.TabIndex = 1;
            this.disRec.Text = "Desabilitar Reconhecedor";
            this.disRec.UseVisualStyleBackColor = true;
            this.disRec.Click += new System.EventHandler(this.disRec_Click);
            // 
            // openPPT
            // 
            this.openPPT.Enabled = false;
            this.openPPT.Location = new System.Drawing.Point(227, 67);
            this.openPPT.Name = "openPPT";
            this.openPPT.Size = new System.Drawing.Size(75, 23);
            this.openPPT.TabIndex = 2;
            this.openPPT.Text = "Abrir PPT";
            this.openPPT.UseVisualStyleBackColor = true;
            this.openPPT.Click += new System.EventHandler(this.openPPT_Click);
            // 
            // recogetionRichTextBox1
            // 
            this.recogetionRichTextBox1.EnableAutoDragDrop = true;
            this.recogetionRichTextBox1.Location = new System.Drawing.Point(25, 115);
            this.recogetionRichTextBox1.Name = "recogetionRichTextBox1";
            this.recogetionRichTextBox1.ReadOnly = true;
            this.recogetionRichTextBox1.Size = new System.Drawing.Size(290, 163);
            this.recogetionRichTextBox1.TabIndex = 4;
            this.recogetionRichTextBox1.Text = "";
            // 
            // openFileDialog_ppt
            // 
            this.openFileDialog_ppt.FileName = "openFileDialog_ppt";
            this.openFileDialog_ppt.Filter = "ppt files|*.ppt";
            this.openFileDialog_ppt.Title = "Abrir PPT";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(348, 317);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.jconfTextBox);
            this.tabPage1.Controls.Add(this.recogetionRichTextBox1);
            this.tabPage1.Controls.Add(this.openPPT);
            this.tabPage1.Controls.Add(this.enRec);
            this.tabPage1.Controls.Add(this.disRec);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(340, 291);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Controller";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Abrir Jconf";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "jConf:";
            // 
            // jconfTextBox
            // 
            this.jconfTextBox.Location = new System.Drawing.Point(38, 18);
            this.jconfTextBox.Name = "jconfTextBox";
            this.jconfTextBox.Size = new System.Drawing.Size(211, 20);
            this.jconfTextBox.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(340, 291);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sobre";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.MediumBlue;
            this.label1.Location = new System.Drawing.Point(121, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "www.laps.ufpa.br";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(328, 201);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // openFileDialogJConf
            // 
            this.openFileDialogJConf.FileName = "Abrir JConf";
            this.openFileDialogJConf.Filter = "julius configuration files|*.jConf";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 341);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "PPT Controller";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button enRec;
        private System.Windows.Forms.Button disRec;
        private System.Windows.Forms.Button openPPT;
        private System.Windows.Forms.RichTextBox recogetionRichTextBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog_ppt;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox jconfTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialogJConf;
    }
}

