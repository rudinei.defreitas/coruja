﻿using System;
using System.Windows.Forms;
using LapsAPI;

namespace pptBR
{
    public partial class Main : Form
    {

        //SR sr;
        SREngine re;
        PPTPresentation ppt;

        private delegate void CrossThread(string text);

        public Main()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //Adiciona sr_onRecognition como função a ser chamada quando for reconhecido alguma coisa
                SREngine.OnRecognition += sr_onRecognition;
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void enRec_Click(object sender, EventArgs e)
        {
            try
            {
                //Habilita o reconhecimento
                re.startRecognition();
                enRec.Enabled = false;
                disRec.Enabled = true;
            }
            catch (Exception e1)
            {
                enRec.Enabled = true;
                disRec.Enabled = false;
                MessageBox.Show(e1.Message);
            }
        }

        void sr_speechReady()
        {
            recogetionRichTextBox1.AppendText("Reconhecendo\n");
        }

        /// <summary>
        /// Evento disparado quando se reconhece alguma sentença
        /// </summary>
        void sr_onRecognition(RecoResult result)
        {
            try
            {
                //Obtem o resultado a partir do result
                string jMessage = result.getUterrance();
                //Formata o resultado
                jMessage = jMessage.Trim();
                jMessage = jMessage.TrimEnd();
                string message = "";
                message = jMessage.Substring(jMessage.IndexOf("<s>") + 3, jMessage.IndexOf("</s>") - 4);
                message = message.Trim();
                message = message.TrimEnd();

                setTextBoxText("sentença: " + message + " Confiança: " + result.getConfidence() + "\n");
                //Chama ações para fazer o que o comando diz se a confiaça for maior que 70%
                if (result.getConfidence() > 0.7)
                {
                    Actions(message);
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        public void setTextBoxText(string text)
        {
            if (this.recogetionRichTextBox1.InvokeRequired)
            {
                CrossThread d = new CrossThread(setText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                recogetionRichTextBox1.AppendText(text);
            }
        }

        public void setText(string text)
        {
            recogetionRichTextBox1.AppendText(text);
        }

        /// <summary>
        /// Ações que a apresentação pode fazer
        /// </summary>
        /// <param name="message"></param>nome do comando a ser processado
        public void Actions(string message)
        {
            try
            {
                switch (message)
                {
                    case "mostrar":
                        ppt.showPresentation();
                        break;
                    case "fechar":
                        ppt.Close();
                        if (this.enRec.InvokeRequired)
                        {
                            CrossThread d = new CrossThread(disabilitarRe);
                            this.Invoke(d, new object[] { "reoff" });
                        }
                        else
                        {
                            disabilitarRe("");
                        }
                        break;
                    case "próximo":
                    case "avançar":
                        ppt.next();
                        break;
                    case "anterior":
                    case "voltar":
                        ppt.previous();
                        break;
                    case "primeiro":
                        ppt.getView().First();
                        break;
                    case "último":
                        ppt.getView().Last();
                        break;
                }
            }
            catch (System.Runtime.InteropServices.COMException e1)
            {
                MessageBox.Show("Você deve abrir um arquivo do power point para poder dar comandos");
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void disRec_Click(object sender, EventArgs e)
        {
            disabilitarRe("");
        }

        private void disabilitarRe(string text)
        {
            try
            {
                //Para de reconhecer
                re.stopRecognition();
                disRec.Enabled = false;
                if (text.Equals("reoff"))
                {
                    enRec.Enabled = false;
                }
                else
                {
                    enRec.Enabled = true;
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void openPPT_Click(object sender, EventArgs e)
        {
            try
            {
                //Abre uma janela para procurar o arquivo de apresentação
                if (openFileDialog_ppt.ShowDialog() == DialogResult.OK)
                {
                    ppt = new PPTPresentation(openFileDialog_ppt.FileName);
                    ppt.openPPT();
                    openPPT.Enabled = false;
                    enRec.Enabled = true;
                }
            }
            catch (Exception e1)
            {
                enRec.Enabled = false;
                ppt.Close();
                MessageBox.Show(e1.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Abre uma janela para procurar o arquivo de configuração
                if (openFileDialogJConf.ShowDialog() == DialogResult.OK)
                {
                    re = new SREngine(openFileDialogJConf.FileName);
                    re.OnSpeechReady += sr_speechReady;
                    jconfTextBox.Text = openFileDialogJConf.FileName;
                    openPPT.Enabled = true;
                }
            }
            catch (Exception e1)
            {
                enRec.Enabled = false;
                MessageBox.Show(e1.Message);
            }
        }
    }
}
