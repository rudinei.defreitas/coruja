﻿O texto abaixo descreve as modificações feitas no julius e os passos para compilá-lo, todos esses passos com excessão de copiar para a pasta do sistema as dll's necessárias já foram feitos na solução do visual estudio que está em /projetos...

Modificações feitas no julius 4.1.2
stddefs.h
ln 54 - Comentada (include de strings.h vc++ não da suporte a strings.h)
recogmain.c
int j_recognize_stream(Recog *recog)
ln 1438 - Adiciona return quando retorna 1 para voltar a função que a chamou e pausar. (callback finaliza e callback pause não irão funcionar)

Compilando o julius para ser compatível com o cl (vc++)

 - Instalar mingw
 - Adicionar a opção -shared (multiplataforma dll) para o compilador (a saida será uma dll -o X.dll)
 - Adicionar a flag seguir para gerar os arquivos de definição (X.def) e biblioteca estatica (X.a) necessário para o compilador cl compatibilizar as bibliotecas do gcc com as deles -Wl,--output-def,X.def,--out-implib,X.a
 - Usar o lib (lib.exe) do vc++ para gerar as bibliotecas do windows (.lib) com o comando lib /MACHINE:X86 /DEF:X.def
 - A dll X.dll deve estar junto com o executável ou em uma pasta do path de busca de dll para que o windows a encontre quando for executar (windows\system)
  
  Isso está implicito no arquivo makeLibXDll em libjulius e em libsent para compilar usando make usa-se make -f makeLibXDll (compila-se primeiro libjulius) O arquivos do makefile copia a dll para f:\windows\system caso sua unidade seja diferente de f modifique a variavél SYSTEM do arquivo para conter seu diretório
  
 - Quando for compilar o programa usando o cl passar para ele o .lib gerado anteriormente e os includes em julius4\libjulius\include e julius4\libsent\include além da macro BUILD_JULIAN
 
 Pedro Batista - pedro@ufpa.br