﻿namespace UseLapsAPIVisual
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.start = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.stop = new System.Windows.Forms.Button();
            this.jConfTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.openJConf = new System.Windows.Forms.Button();
            this.openJconfDialog = new System.Windows.Forms.OpenFileDialog();
            this.closeJconf = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.Enabled = false;
            this.start.Location = new System.Drawing.Point(49, 80);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(75, 23);
            this.start.TabIndex = 0;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(49, 121);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(292, 183);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // stop
            // 
            this.stop.Enabled = false;
            this.stop.Location = new System.Drawing.Point(266, 80);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 23);
            this.stop.TabIndex = 2;
            this.stop.Text = "Stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.button2_Click);
            // 
            // jConfTextBox
            // 
            this.jConfTextBox.Location = new System.Drawing.Point(86, 37);
            this.jConfTextBox.Name = "jConfTextBox";
            this.jConfTextBox.ReadOnly = true;
            this.jConfTextBox.Size = new System.Drawing.Size(174, 20);
            this.jConfTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "jConf:";
            // 
            // openJConf
            // 
            this.openJConf.Location = new System.Drawing.Point(266, 35);
            this.openJConf.Name = "openJConf";
            this.openJConf.Size = new System.Drawing.Size(75, 23);
            this.openJConf.TabIndex = 5;
            this.openJConf.Text = "Open jConf";
            this.openJConf.UseVisualStyleBackColor = true;
            this.openJConf.Click += new System.EventHandler(this.openJConf_Click);
            // 
            // openJconfDialog
            // 
            this.openJconfDialog.FileName = "jConf";
            this.openJconfDialog.Filter = "jConf files | *.jConf; *.jconf | All files | *.*";
            // 
            // closeJconf
            // 
            this.closeJconf.Location = new System.Drawing.Point(151, 80);
            this.closeJconf.Name = "closeJconf";
            this.closeJconf.Size = new System.Drawing.Size(75, 23);
            this.closeJconf.TabIndex = 6;
            this.closeJconf.Text = "close jConf";
            this.closeJconf.UseVisualStyleBackColor = true;
            this.closeJconf.Click += new System.EventHandler(this.closeJconf_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 335);
            this.Controls.Add(this.closeJconf);
            this.Controls.Add(this.openJConf);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.jConfTextBox);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.TextBox jConfTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button openJConf;
        private System.Windows.Forms.OpenFileDialog openJconfDialog;
        private System.Windows.Forms.Button closeJconf;
    }
}

