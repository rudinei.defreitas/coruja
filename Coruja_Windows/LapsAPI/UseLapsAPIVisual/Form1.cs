﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UseLapsAPIVisual
{
    public partial class Form1 : Form
    {
        LapsAPI.SREngine re;

        private delegate void setTextCallback(string text);

        public Form1()
        {
            InitializeComponent();
        }

        private void onSpeechReady()
        {
            richTextBox1.AppendText("Reconhecendo\n");
        }

        private void onRecognized(LapsAPI.RecoResult result)
        {
            if (this.richTextBox1.InvokeRequired)
            {
                setTextCallback d = new setTextCallback(setText);
                this.Invoke(d, new object[] { "Sentença: " + result.getUterrance() + " Confiaça: " + result.getConfidence() + "\n" });
            }
            else
            {
                richTextBox1.AppendText("Sentença: " + result.getUterrance() + " Confiaça: " + result.getConfidence() + "\n");
            }
        }

        private void setText(string text)
        {
            richTextBox1.AppendText(text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                re.startRecognition();
                start.Enabled = false;
                stop.Enabled = true;
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
                MessageBox.Show(e1.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                re.stopRecognition();
                start.Enabled = true;
                stop.Enabled = false;
                richTextBox1.AppendText("Não reconhecendo\n");
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.ToString());
                MessageBox.Show(e1.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void openJConf_Click(object sender, EventArgs e)
        {
            try
            {
                if (openJconfDialog.ShowDialog() == DialogResult.OK)
                {
                    re = new LapsAPI.SREngine(openJconfDialog.FileName);

                    LapsAPI.SREngine.OnRecognition += onRecognized;
                    re.OnSpeechReady += onSpeechReady;
                    start.Enabled = true;
                    jConfTextBox.Text = openJconfDialog.FileName;
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void closeJconf_Click(object sender, EventArgs e)
        {
            re.dSREngine();
        }
    }
}
